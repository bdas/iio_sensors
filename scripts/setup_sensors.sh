#!/bin/bash

rmmod spi_imu
rmmod bmi160_spi
rmmod bmi160_core
insmod /home/bdas/extra_modules/extra/spi_imu.ko
modprobe bmi160_core
modprobe bmi160_spi


echo "Printing name for iio device 0"
cat /sys/bus/iio/devices/iio:device0/name


rmmod bmc150_magn_i2c
rmmod bmc150_magn_core
modprobe bmc150_magn_core
modprobe bmc150_magn_i2c
echo bmc150_magn 0x12 > /sys/bus/i2c/devices/i2c-2/new_device

echo "Printing name for iio device 1"
cat /sys/bus/iio/devices/iio:device1/name
