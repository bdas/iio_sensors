#!/bin/bash
echo "Loadinng IIO modules"
cd /lib/modules/4.13.0-16-generic/kernel/drivers/iio
modprobe industrialio_configfs
modprobe industrialio
modprobe industrialio_sw_device
modprobe industrialio_sw_trigger
modprobe industrialio_triggered_event
modprobe iio_trig_hrtimer
cd -

echo "Setting up configfs entries"
mkdir /sys/kernel/config/iio/triggers/hrtimer/trigger0
echo 5000 > /sys/bus/iio/devices/trigger0/sampling_frequency


echo "Setting up triggers for IIO device"
cd /sys/bus/iio/devices/iio:device0
echo trigger0 > trigger/current_trigger
#cd /sys/bus/iio/devices/iio:device1
#echo trigger0 > trigger/current_trigger

#echo "Enabling scan elements"
#echo 1 > scan_elements/in_accel_x_en
#echo 1 > scan_elements/in_accel_y_en
#echo 1 > scan_elements/in_accel_z_en
#echo 1 > buffer/enable


